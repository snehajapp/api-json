import { Component } from '@angular/core';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  getUrl = 'https://api.myjson.com/bins/sbnyq';
  postUrl = 'https://api.myjson.com/bins';
  putUrl = 'https://api.myjson.com/bins/dhz0';

  getUrl1 = 'https://api.jsonbin.io/b/5cde5d0bdbffad51f8aafdfb';
  postUrl1 = 'https://api.jsonbin.io/b';
  putUrl1 = 'https://api.jsonbin.io/b/5cde5d0bdbffad51f8aafdfb';

  secretKey = '$2a$10$k9oWh/xfoMdwuR.xZN3vfed2gp1VuXcZ9uU8O5md3jipILxunsiSe';
  headers = {
    'content-type': 'application/json',
    'secret-key': this.secretKey,
    'private': 'true'
  }

  data = { "a": 5 };

  testData = null;//[{
  //   "id": "4767",
  //   "employee_name": "00000000000000",
  //   "employee_salary": "456456456",
  //   "employee_age": "456456456",
  //   "profile_image": ""
  // },
  // {
  //   "id": "4768",
  //   "employee_name": "samay 73874639-1ac1-43c4-8b6b-9a806a569f7d ",
  //   "employee_salary": "123",
  //   "employee_age": "33",
  //   "profile_image": ""
  // },
  // {
  //   "id": "4770",
  //   "employee_name": "Cory Hand",
  //   "employee_salary": "1081",
  //   "employee_age": "36",
  //   "profile_image": ""
  // },
  // {
  //   "id": "4771",
  //   "employee_name": "Miss Jin Roberts",
  //   "employee_salary": "1027",
  //   "employee_age": "40",
  //   "profile_image": ""
  // }
  // ];

  constructor(private commonService: CommonService) {

  }

  ngOnInit() {
    //this.createJSON(this.postUrl1, this.data, this.headers);//5cde5d0bdbffad51f8aafdfb
    // this.loadJSON(this.getUrl1, this.headers);
    //this.updateJSON(this.putUrl1, this.data, this.headers);
    //this.deleteJSON(this.putUrl1, this.headers);

    //this.loadJSON("assets/data.json");
    this.loadJSON("http://dummy.restapiexample.com/api/v1/employees");
    //http://dummy.restapiexample.com/api/v1/employees
  }

  loadJSON(url: string, headers?: any) {
    this.commonService.getJSON(url, headers).subscribe((res) => {
      console.log(res);
      this.testData = res;
    }, (err) => {
      console.log(err);
    });
  }

  createJSON(url: string, data: any, headers?: any) {
    this.commonService.postJSON(url, data, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  updateJSON(url: string, data: any, headers?: any) {
    this.commonService.putJSON(url, data, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  deleteJSON(url: string, headers?: any) {
    this.commonService.deleteJSON(url, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }
}
